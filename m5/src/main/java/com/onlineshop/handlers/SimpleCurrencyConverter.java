package com.onlineshop.handlers;

import java.util.HashMap;
import java.util.Map;

public class SimpleCurrencyConverter {

    private HttpHelper httpHelper = new HttpHelper();
    private final String currencyTo;

    Map<String, Double> convertorFactor = new HashMap<>();

    public SimpleCurrencyConverter(String currencyTo){
        initConvertorFactor();
        this.currencyTo = currencyTo;
    }

    private void initConvertorFactor() {
        this.convertorFactor.put("EUR", 0.9);
        this.convertorFactor.put("CAD", 1.35);
    }

    public double convert(double price){
        if(convertorFactor.containsKey(currencyTo)){
            return price * convertorFactor.get(currencyTo);
        } else {
            throw new IllegalArgumentException("Unrecognized currency : " + currencyTo);
        }
    }

    public double convertWithWebService(double price){
        String rates = httpHelper.getCurrencyRate("USD");

        if(currencyTo.equalsIgnoreCase("EUR")){
            return price * getRate(rates, "EUR");
        } else if (currencyTo.equalsIgnoreCase("CAD")) {
            return price * getRate(rates, "CAD");
        } else {
            throw new IllegalArgumentException("Unrecognized currency : " + currencyTo);
        }
    }

    public void printUsdRateFor(String currencyTo){
        String rates = httpHelper.getCurrencyRate("USD");
        System.out.println(getRate(rates, currencyTo));
    }

    private double getRate(String rates, String currencyTo){
        return Double.parseDouble(rates.substring(rates.lastIndexOf(currencyTo)).substring(5,9));
    }
}
