package com.onlineshop.entities;

import java.time.LocalDate;
import java.util.regex.Pattern;

import static org.testng.AssertJUnit.assertTrue;

public class Voucher {

    String code;

    LocalDate startDate;
    LocalDate expiryDate;

    public Voucher() {
    }

    public Voucher(String code){
        //add validation here too
        this.code = code;
    }

    public String getCode(){
        return code;
    }

    public void setCode(String code){
        assertTrue(Pattern.compile("^[a-zA-Z0-9]+$").matcher(code).matches());
        this.code = code;
    }
}
