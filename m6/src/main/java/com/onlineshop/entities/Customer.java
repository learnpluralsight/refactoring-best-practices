package com.onlineshop.entities;

public class Customer {

    private Membership membership;
    private Address address;
    private Phone phone;
    private int age;

    public Customer(Address address){
        this.address = address;
    }

    public Customer(Address address, int age){
        this.address = address;
        this.age = age;
    }

    public Customer(Membership membership, Address address, int age){
        this.membership = membership;
        this.address = address;
        this.age = age;
    }

    public Membership getMembership() {
        return membership;
    }

    public Address getAddress() {
        return address;
    }

    public int getAge() {
        return age;
    }

    public int getInternationalPhoneNumber(){
        return Integer.parseInt(
                phone.getInternationalFormat()
        );
    }

    public int getSimplePhoneNumber(){
        return Integer.parseInt(
                phone.getSimpleFormat()
        );
    }

    public String getCountry(){
        return this.getAddress().getCountry().toString();
    }
}
