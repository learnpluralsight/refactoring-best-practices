package com.onlineshop.handlers;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class HttpHelper {
    private static final String EXCHANGE_RATE_API = "https://api.exchangeratesapi.io/latest?base=";

    public String getCurrencyRate(String baseCurrency){
        HttpClient httpClient = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(EXCHANGE_RATE_API+baseCurrency))
                .build();

        HttpResponse<String> response = null;
        try{
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException |InterruptedException e){
            e.printStackTrace();
        }
        return response.body();
    }
}
