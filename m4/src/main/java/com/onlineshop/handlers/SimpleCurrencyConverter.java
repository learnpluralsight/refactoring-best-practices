package com.onlineshop.handlers;

import java.util.HashMap;
import java.util.Map;

public class SimpleCurrencyConverter {

    private final String currencyTo;

    Map<String, Double> convertorFactor = new HashMap<>();

    public SimpleCurrencyConverter(String currencyTo){
        initConvertorFactor();
        this.currencyTo = currencyTo;
    }

    private void initConvertorFactor() {
        this.convertorFactor.put("EUR", 0.9);
        this.convertorFactor.put("CAD", 1.35);
    }

    public double convert(double price){
        if(convertorFactor.containsKey(currencyTo)){
            return price * convertorFactor.get(currencyTo);
        } else {
            throw new IllegalArgumentException("Unrecognized currency : " + currencyTo);
        }
    }
}
